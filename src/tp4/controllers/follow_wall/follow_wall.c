#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <webots/robot.h>
#include <webots/differential_wheels.h>
#include <webots/distance_sensor.h>

#define private static

#define TIME_STEP 64

//		3	3
// 	3			3
//	3			3
//		0	0
#define STATES_COUNT 	729

#define ACTIONS_COUNT 	3
#define ACTIONSxSTATES 	(STATES_COUNT * ACTIONS_COUNT)
#define IRS_COUNT 		8
#define SENSORS_COUNT 	6


#define ROTATE_SPEED 		1000
#define ROTATE_STEPS 		1
#define ADVANCE_SPEED 		200
#define ADVANCE_STEPS 		1

#define POLICY_FILE 		"/home/gcastigl/Desktop/tp4/policy.txt"
#define Q_FILE 				"/home/gcastigl/Desktop/tp4/Q.txt"

#define SQRT2 1.4142

#define THREE_LAYER_LOW_SIDE			500
#define THREE_LAYER_MED_SIDE			1000

#define THREE_LAYER_LOW_DIAG  THREE_LAYER_LOW_SIDE / SQRT2
#define THREE_LAYER_MED_DIAG  THREE_LAYER_MED_SIDE / SQRT2

#define THREE_LAYER_LOW_FRONT 80
#define THREE_LAYER_MED_FRONT 800

#define MAX_ITERATIONS 	5000
#define CICLES 1

typedef struct {
	int sensors[SENSORS_COUNT];
} State;

typedef struct {
	void (*fp)();
	int delaySteps;
} Action;

typedef struct {
	float Q[STATES_COUNT][ACTIONS_COUNT];
	State states[STATES_COUNT];
	Action actions[ACTIONS_COUNT];
	double epsilon;
	double gamma;
	double alpha;
	long maxIterations;
	int remainingSteps;
} QLearning;

private void initQ(QLearning* qlearning);
private void createStates(QLearning* qlearning);
private void createActions(QLearning* qlearning);
private void loadQFromFile(QLearning* qlearning);

private void rotateLeft();
private void rotateRight();
private void advance();

private int readState(QLearning* qlearning, WbDeviceTag irs[IRS_COUNT]);
private int step(QLearning* qlearning, int currentStateIndex);

private int indexForState(QLearning* qlearning, State* s);

private float r(QLearning* qlearning, int prevStateIndex, int actionIndex, int stateIndex);
private float maxQValue(QLearning* qlearning, int sIndex);
private void decrementEpsilon(QLearning* qlearning);
private void updateQMatrix(QLearning* qlearning, int spIndex, int aIndex, int sIndex);

// DEBUG
private void debug_states(QLearning* qlearning);
private void debug_q(QLearning* qlearning);

// IO
private void saveToFile(QLearning* qlearning);
private void saveAsPolicy(QLearning* qlearning);

int main()
{
	srand(time(NULL));
	wb_robot_init(); /* necessary to initialize webots stuff */
	WbDeviceTag irs[IRS_COUNT];
	char* irNames[IRS_COUNT] = {"ps0", "ps1", "ps2", "ps3", "ps4", "ps5", "ps6", "ps7"};
	int i;
	for (i = 0; i < IRS_COUNT; i++) {
		WbDeviceTag ir = wb_robot_get_device(irNames[i]);
		wb_distance_sensor_enable(ir, TIME_STEP);	
		irs[i] = ir;
	}
	// Configure qLearning
	QLearning qLearning;
	initQ(&qLearning);
	int sIndex, aIndex, spIndex = -1;
  time_t start = time(NULL);
	for (i = 0 ; i < CICLES ; i++) {
		printf("Starting cicle %d\n", i);
		qLearning.remainingSteps = 0;
		qLearning.epsilon = 1;
		long iterations = 0;
		loadQFromFile(&qLearning);
		while(wb_robot_step(TIME_STEP) != -1 && iterations < qLearning.maxIterations) {
			if (qLearning.remainingSteps > 0) {
				// La accion todavia no se termino de realizar
				qLearning.actions[aIndex].fp();
				qLearning.remainingSteps--;
			} else {
				if (iterations % 100 == 0) {
					int p = (i * qLearning.maxIterations + iterations) * 100.0 / CICLES / qLearning.maxIterations;
          int elapsed = (int) time(NULL) - (int) start;
          float total_seconds_remaining = (100 - p) * 1.0 * elapsed / p;
          int minutes_remaining = total_seconds_remaining / 60;
          int seconds_remaining = total_seconds_remaining - 60 * minutes_remaining;
          printf("[Q-Learning] Completado: %d\n", p);
          if (p == 0) {
           printf("Tiempo estimado: desconocido");
          } else {
            printf("Tiempo estimado: %02d:%02d", minutes_remaining, seconds_remaining);
          }
				}
				sIndex = readState(&qLearning, irs);
				if (sIndex == -1) {
					printf("[ERROR] Estado no definido!! - Abort");
					return -1;
				}
				aIndex = step(&qLearning, sIndex);
				Action action = qLearning.actions[aIndex];
				qLearning.remainingSteps = action.delaySteps;
				if (spIndex != -1) {	// La primera vez va a ser -1 nada mas
					updateQMatrix(&qLearning, spIndex, aIndex, sIndex);
				}
				spIndex = sIndex;
				iterations++;
			}
		}
		saveToFile(&qLearning);
		saveAsPolicy(&qLearning);
	}
  return 0;
}

private void initQ(QLearning* qlearning) {
	int i, j;
	for (i = 0; i < STATES_COUNT; i++) {
		for (j = 0; j < ACTIONS_COUNT; j++) {
			qlearning->Q[i][j] = 0;
		}	
	}
	createStates(qlearning);
	createActions(qlearning);
	qlearning->epsilon = 1;
	qlearning->alpha = 0.1;
	qlearning->gamma = 0.9;
	qlearning->maxIterations = MAX_ITERATIONS;
	qlearning->remainingSteps = 0;
}

private void createStates(QLearning* qlearning) {
	int i1, i2, i3, i4, i5, i6;
	int index = 0;
	for (i1 = 0; i1 < 3; i1++) {
		for (i2 = 0; i2 < 3; i2++) {
			for (i3 = 0; i3 < 3; i3++) {
				for (i4 = 0; i4 < 3; i4++) {
					for (i5 = 0; i5 < 3; i5++) {
						for (i6 = 0; i6 < 3; i6++) {
							State* s = &qlearning->states[index++];
							s->sensors[0] = i1;
							s->sensors[1] = i2;
							s->sensors[2] = i3;
							s->sensors[3] = i4;
							s->sensors[4] = i5;
							s->sensors[5] = i6;
						}
					}
				}
			}
		}
	}
}

private void createActions(QLearning* qlearning) {
	qlearning->actions[0].fp = advance;
	qlearning->actions[0].delaySteps = ADVANCE_STEPS;
	qlearning->actions[1].fp = rotateLeft;
	qlearning->actions[1].delaySteps = ROTATE_STEPS;
	qlearning->actions[2].fp = rotateRight;
	qlearning->actions[2].delaySteps = ROTATE_STEPS;
}

private void rotateLeft() {
//	printf("Rotar 30 grados");
	double right_speed = ROTATE_SPEED, left_speed = -ROTATE_SPEED;
	wb_differential_wheels_set_speed(left_speed, right_speed);
}

private void rotateRight() {
//	printf("Rotar -30 grados");
	double right_speed = -ROTATE_SPEED, left_speed = ROTATE_SPEED;
	wb_differential_wheels_set_speed(left_speed, right_speed);
}

private void advance() {
//	printf("Avanzar x distancia");
	double right_speed = ADVANCE_SPEED, left_speed = ADVANCE_SPEED;
	wb_differential_wheels_set_speed(left_speed, right_speed);
}

private int indexForState(QLearning* qlearning, State* s) {
	int i, j;
	for (i = 0; i < STATES_COUNT; i++) {
		int* other = qlearning->states[i].sensors;
		for (j = 0; j < SENSORS_COUNT; j++) {
			if (other[j] != s->sensors[j]) {
				break;
			}
		}
		if (j == SENSORS_COUNT) {
			return i;
		}
	}
	return -1;
}


// ========================================================================
//				            Lectura de estado actual
// ========================================================================

private int read3_1(double readings[IRS_COUNT], int id, float low, float med) {
  float value = readings[id];
  if (value < low) {
		return 0;
	} else if (value < med) {
		return 1;
	}
	return 2;
}

//private int read2_2(double readings[IRS_COUNT], int id1, int id2) {
//	return read2((readings[id1] + readings[id2]) / 2);
//}

private int readState(QLearning* qlearning, WbDeviceTag irs[IRS_COUNT]) {
	double readings[IRS_COUNT];
	int i;
	for (i = 0; i < IRS_COUNT; i++) {
		readings[i] = wb_distance_sensor_get_value(irs[i]);
	}
	State s;
	int* sensors = s.sensors;
	sensors[0] = read3_1(readings, 5, THREE_LAYER_LOW_SIDE, THREE_LAYER_MED_SIDE);
	sensors[1] = read3_1(readings, 6, THREE_LAYER_LOW_DIAG, THREE_LAYER_MED_DIAG);
	sensors[2] = read3_1(readings, 7, THREE_LAYER_LOW_FRONT, THREE_LAYER_MED_FRONT);
	sensors[3] = read3_1(readings, 0, THREE_LAYER_LOW_FRONT, THREE_LAYER_MED_FRONT);
	sensors[4] = read3_1(readings, 1, THREE_LAYER_LOW_DIAG, THREE_LAYER_MED_DIAG);
	sensors[5] = read3_1(readings, 2, THREE_LAYER_LOW_SIDE, THREE_LAYER_MED_SIDE);
	return indexForState(qlearning, &s);
}

// =========================================================================

private int step(QLearning* qlearning, int stateIndex) {
	int i;
	int bestActionIndex;
	int r = rand() / (float) RAND_MAX;
	if (r < qlearning->epsilon) {
		// explore
		return ((rand() / (float) RAND_MAX) * ACTIONS_COUNT);
	} else {
		// exploit
		bestActionIndex = 0;
		for (i = 0; i < ACTIONS_COUNT; i++) {
			float value = qlearning->Q[stateIndex][i];
			if (value > qlearning->Q[stateIndex][bestActionIndex]) {
				bestActionIndex = i;
			}
		}
		return bestActionIndex;
	}
}


private float r(QLearning* qlearning, int prevStateIndex, int actionIndex, int stateIndex) {
	int* s0 = qlearning->states[prevStateIndex].sensors;
	int* s1 = qlearning->states[stateIndex].sensors;
	float reward = 0;
	if (s1[2] > s0[2]) {
    reward -= 50;
  }
  if (s1[3] > s0[3]) {
    reward -= 50;
  }
  if (s1[2] < s0[2]) {
    reward += 25;
  }
  if (s1[3] < s0[3]) {
    reward += 25;
  }
  if (s1[2] == 0 && s1[3] == 0 && actionIndex == 0) {
    reward += 25;
  }
  if (s1[0] == 1 && s1[1] == 1) {
    reward += 25;
  }
  if (s0[0] == 1 && (s1[0] != 1 || s1[1] == 0)) {
    reward -= 50;
  }
	if (s1[5] == 1 && s1[4] == 1) {
    reward += 25;
  }
  if (s0[5] == 1 && (s1[5] != 1 || s1[4] == 0)) {
    reward -= 50;
  }
  
//	printf("StateInx: %d -> %d\n", prevStateIndex, stateIndex);
//	printf("[%d, %d, %d, %d, %d]\n", s1[0], s1[1], s1[2], s1[3], s1[4]);
	if (reward != 0)
		printf("Reward: %f\n", reward);
	return reward;
}


private float maxQValue(QLearning* qlearning, int sIndex) {
	int i, maxAIndex = 0;
	for (i = 0; i < ACTIONS_COUNT; i++) {
		float value = qlearning->Q[sIndex][i];
		if (value > qlearning->Q[sIndex][maxAIndex]) {
			maxAIndex = i;
		}
	}
	return qlearning->Q[sIndex][maxAIndex];
}

private void updateQMatrix(QLearning* qLearning, int spIndex, int aIndex, int sIndex) {
	float alpha = qLearning->alpha;
	float gamma = qLearning->gamma;
	float qspa = qLearning->Q[spIndex][aIndex];
	float reinf = r(qLearning, spIndex, aIndex, sIndex);
	float maxa = maxQValue(qLearning, sIndex);
	qLearning->Q[spIndex][aIndex] = (1 - alpha) * qspa + alpha * (reinf + gamma * maxa);
	// printf("%d + %d = %f", spIndex, aIndex, qLearning->Q[spIndex][aIndex]);
	decrementEpsilon(qLearning);
}

private void decrementEpsilon(QLearning* qlearning) {
	if (qlearning->epsilon >= 0.2) {
		qlearning->epsilon -= 1 / (float) qlearning->maxIterations;
	}
}

private void debug_states(QLearning* qlearning) {
	int i, j;
	for (i = 0; i < STATES_COUNT; i++) {
		printf("%d\t\t", i);
		for (j = 0; j < SENSORS_COUNT; j++) {
			printf("%s%d\t", i < 10 ? " " : "", qlearning->states[i].sensors[j]);
		}
		printf("\n");
	}
}

private void debug_q(QLearning* qlearning) {
	int i, j;
	for (i = 0; i < STATES_COUNT; i++) {
		printf("%d\t\t", i);
		for (j = 0; j < ACTIONS_COUNT; j++) {
			printf("%f\t", qlearning->Q[i][j]);
		}
		printf("\n");
	}
}

// =============================================================================
// 							Export / Import
// =============================================================================

private void saveToFile(QLearning* qlearning) {
	FILE * f = fopen(Q_FILE, "w");
	if (f == NULL) {
    	printf("Error opening file!\n");
	    exit(1);
	}
	int i, j;
	for (i = 0; i < STATES_COUNT; i++) {
		for (j = 0; j < ACTIONS_COUNT; j++) {
			fprintf(f, "%f\t", qlearning->Q[i][j]);
		}
		fprintf(f, "\n");
	}
	fclose(f);
}

private void loadQFromFile(QLearning* qlearning) {
	FILE *f = fopen(Q_FILE, "rt");
	if (f == NULL) {
    	printf("No se encontro archivo para Matriz Q!\n");
	    exit(1);
	}
	printf("Cargando matriz Q de archivo!!");
	char line[80];
	int stateIndex = 0;
	float a1, a2, a3;
	while (fgets(line, 80, f) != NULL) {
		sscanf(line, "%f\t%f\t%f\n", &a1, &a2, &a3);
		printf("%f\t%f\t%f\n", a1, a2, a3);
		qlearning->Q[stateIndex][0] = a1;
		qlearning->Q[stateIndex][1] = a2;
		qlearning->Q[stateIndex][2] = a3;
		stateIndex++;
	}
	fclose(f);
}


private void saveAsPolicy(QLearning* qlearning) {
	FILE *f = fopen(POLICY_FILE, "w");
	if (f == NULL) {
    	printf("Error opening file!\n");
	    exit(1);
	}
	int i, j, maxAIndex = 0;
	for (i = 0; i < STATES_COUNT; i++) {
		maxAIndex = 0;
		int visited = 0;
		for (j = 0; j < ACTIONS_COUNT; j++) {
			float value = qlearning->Q[i][j];
			if (value > qlearning->Q[i][maxAIndex]) {
				maxAIndex = j;
			}
			if (value != 0) {
				visited = 1;
			}
		}
		if (visited == 1) {
			int* s = qlearning->states[i].sensors;
			fprintf(f, "%d\t%d\t%d\t%d\t%d\t%d\t%d\n", 
				s[0], s[1], s[2], s[3], s[4], s[5], maxAIndex);
		}
	}
	fclose(f);
}

