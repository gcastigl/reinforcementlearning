#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <webots/robot.h>
#include <webots/differential_wheels.h>
#include <webots/distance_sensor.h>

#define private static
#define TIME_STEP 64

//		3	3
// 	3			3
//	3			3
//		0	0
#define STATES_COUNT 	729

#define ACTIONS_COUNT 	3
#define ACTIONSxSTATES 	(STATES_COUNT * ACTIONS_COUNT)
#define IRS_COUNT 		8
#define SENSORS_COUNT 	6


#define ROTATE_SPEED 		1000
#define ROTATE_STEPS 		1
#define ADVANCE_SPEED 		200
#define ADVANCE_STEPS 		1

#define POLICY_FILE 		"/home/jorge/Desktop/policy.txt"

#define SQRT2 1.4142

#define THREE_LAYER_LOW_SIDE			500
#define THREE_LAYER_MED_SIDE			1000

#define THREE_LAYER_LOW_DIAG  THREE_LAYER_LOW_SIDE / SQRT2
#define THREE_LAYER_MED_DIAG  THREE_LAYER_MED_SIDE / SQRT2

#define THREE_LAYER_LOW_FRONT 80
#define THREE_LAYER_MED_FRONT 800

typedef struct {
	int sensors[SENSORS_COUNT];
} State;

typedef struct {
	void (*fp)();
	int delaySteps;
} Action;

typedef struct {
	State state;
	Action action;
} PolicyEntry;

// IO
private void loadFromFile(PolicyEntry* policy);

// Acions
private void rotateLeft();
private void rotateRight();
private void advance();

// Util
private int readState(PolicyEntry* policy, WbDeviceTag irs[IRS_COUNT]);
private int indexForState(PolicyEntry* policy, State* s);

int main()
{
	srand(time(NULL));
	wb_robot_init(); /* necessary to initialize webots stuff */
	WbDeviceTag irs[IRS_COUNT];
	char* irNames[IRS_COUNT] = {"ps0", "ps1", "ps2", "ps3", "ps4", "ps5", "ps6", "ps7"};
	int i;
	for (i = 0; i < IRS_COUNT; i++) {
		WbDeviceTag ir = wb_robot_get_device(irNames[i]);
		wb_distance_sensor_enable(ir, TIME_STEP);	
		irs[i] = ir;
	}
	PolicyEntry policy[STATES_COUNT];
	loadFromFile(policy);
	int remainingSteps = 0, stateIndex;
	while(wb_robot_step(TIME_STEP) != -1) {
		if (remainingSteps > 0) {
			policy[stateIndex].action.fp();
			remainingSteps--;
		} else {
			stateIndex = readState(policy, irs);
			if (stateIndex == -1) {	// no fue visitado
				printf("Estado no visitado\n");
				stateIndex = ACTIONS_COUNT * (rand() / (float) RAND_MAX);
			}
		    printf("STATE: %d\n", stateIndex);
			remainingSteps = policy[stateIndex].action.delaySteps;
		}
	}
  	return 0;
}

private void rotateLeft() {
//	printf("Rotar 30 grados");
	double right_speed = ROTATE_SPEED, left_speed = -ROTATE_SPEED;
	wb_differential_wheels_set_speed(left_speed, right_speed);
}

private void rotateRight() {
//	printf("Rotar -30 grados");
	double right_speed = -ROTATE_SPEED, left_speed = ROTATE_SPEED;
	wb_differential_wheels_set_speed(left_speed, right_speed);
}

private void advance() {
//	printf("Avanzar x distancia");
	double right_speed = ADVANCE_SPEED, left_speed = ADVANCE_SPEED;
	wb_differential_wheels_set_speed(left_speed, right_speed);
}

private int indexForState(PolicyEntry* policy, State* s) {
	int i, j;
	for (i = 0; i < STATES_COUNT; i++) {
		int* other = policy[i].state.sensors;
		for (j = 0; j < SENSORS_COUNT; j++) {
			if (other[j] != s->sensors[j]) {
				break;
			}
		}
		if (j == SENSORS_COUNT) {
			return i;
		}
	}
	return -1;
}


// ========================================================================
//				            Lectura de estado actual
// ========================================================================

private int read3_1(double readings[IRS_COUNT], int id, float low, float med) {
  float value = readings[id];
  if (value < low) {
		return 0;
	} else if (value < med) {
		return 1;
	}
	return 2;
}

/*
private int read2_2(double readings[IRS_COUNT], int id1, int id2) {
	return read2((readings[id1] + readings[id2]) / 2);
}
*/

private int readState(PolicyEntry* policy, WbDeviceTag irs[IRS_COUNT]) {
	double readings[IRS_COUNT];
	int i;
	for (i = 0; i < IRS_COUNT; i++) {
		readings[i] = wb_distance_sensor_get_value(irs[i]);
	}
	State s;
	int* sensors = s.sensors;
	sensors[0] = read3_1(readings, 5, THREE_LAYER_LOW_SIDE, THREE_LAYER_MED_SIDE);
	sensors[1] = read3_1(readings, 6, THREE_LAYER_LOW_DIAG, THREE_LAYER_MED_DIAG);
	sensors[2] = read3_1(readings, 7, THREE_LAYER_LOW_FRONT, THREE_LAYER_MED_FRONT);
	sensors[3] = read3_1(readings, 0, THREE_LAYER_LOW_FRONT, THREE_LAYER_MED_FRONT);
	sensors[4] = read3_1(readings, 1, THREE_LAYER_LOW_DIAG, THREE_LAYER_MED_DIAG);
	sensors[5] = read3_1(readings, 2, THREE_LAYER_LOW_SIDE, THREE_LAYER_MED_SIDE);
	int index = indexForState(policy, &s);
	if (index == -1) {
		printf("NO VISITADO: %d\t%d\t%d\t%d\t%d\t%d\t\n", sensors[0], sensors[1], sensors[2], sensors[3], sensors[4], sensors[5]); 
	}
	return index;
}

// =========================================================================

// =============================================================================
// 							Export / Import
// =============================================================================

private void loadFromFile(PolicyEntry* policy) {
	char line[80];
	FILE* fr = fopen(POLICY_FILE, "rt");
	int s0, s1, s2, s3, s4, s5, actionIndex;
	int index = 0;
	while (fgets(line, 80, fr) != NULL) {
		sscanf(line, "%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &s0, &s1, &s2, &s3, &s4, &s5, &actionIndex);
		printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\n", s0, s1, s2, s3, s4, s5, actionIndex);
		int* sensors = policy[index].state.sensors;
		sensors[0] = s0;
		sensors[1] = s1;
		sensors[2] = s2;
		sensors[3] = s3;
		sensors[4] = s4;
		sensors[5] = s5;
		Action* action = &(policy[index].action);
		if (actionIndex == 0) {
			action->fp = advance;
			action->delaySteps = ADVANCE_STEPS;
		} else if (actionIndex == 1) {
			action->fp = rotateLeft;
			action->delaySteps = ROTATE_STEPS;
		} else if (actionIndex == 2) {
			action->fp = rotateRight;
			action->delaySteps = ROTATE_STEPS;
		}
		index++;
	}
	// int* sensor = policy[0].state.sensor[0];
	// printf("%d %d %d %d %d %d\n", sensor[0], sensor[1], sensor[2], sensor[3], sensor[4], sensor[5]);
	fclose(fr);
}


