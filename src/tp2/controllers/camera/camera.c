/*
 * File:         camera.c
 * Date:         August 9th, 2006
 * Description:  An example of use of a camera device.
 * Author:       Simon Blanchoud
 * Modifications:
 *
 * Copyright (c) 2006 Cyberbotics - www.cyberbotics.com
 */

#include <webots/robot.h>
#include <webots/differential_wheels.h>
#include <webots/camera.h>
#include <stdio.h>

#define SPEED 40
#define TIME_STEP 64
#define COLOR_BLOB_VALUE 380000
#define GREY_BLOB_VALUE 350000
#define THRESHOLD 90

int threshold(int r, int g, int b) {
  if (r >= 2 * THRESHOLD && g < THRESHOLD && b < THRESHOLD) {
    return 1;
  }
  return 0;
}

int isCentered(int redMatrix[52][39], int width, int height, const unsigned char * image) {
  int i, j, red, green, blue, invalidRow, reds;
  int errors = 0;
  for (i = 0 ; i < height ; i++) {
    for (j = 0 ; j < width ; j++) {
      red = wb_camera_image_get_red(image, width, j, i);
      green = wb_camera_image_get_green(image, width, j, i);
      blue = wb_camera_image_get_blue(image, width, j, i);
      redMatrix[j][i] = threshold(red, green, blue);
    }
    
    
    for (j = 0 ; j < width / 2 ; j++) {
      if (redMatrix[j][i] != redMatrix[width - j][i]) {
        errors++;
      }
    }
  }
  if (errors >= height * width / 10) {
      return false;
    }
  // Checks that the center is red.
  for (i = (width - 1)/ 2  - 2; i < (width - 1) / 2 + 2 ; i++) {
    for (j = (height - 1) / 2 - 2 ; j < (height - 1) / 2 + 2 ; j++) {
      if (!redMatrix[i][j]) {
        return false;
      }
    }
  }
  for (i = 0 ; i < height ; i++) {
    for (j = 0 ; j < width ; j++) {
      printf("%d ", redMatrix[j][i]);
    }
    printf("\n");
  }
  return true;
}



int main() {
  WbDeviceTag camera;
  int width, height;
  int pause_counter=0;
  int left_speed, right_speed;
  int i, j, centering_weight;
  int red, blue, green;
  const unsigned char *image;

  wb_robot_init();

  /*
   * First we get a handler to the camera device and then we open and place
   * it. We also store its height and width for further use.
   */
  camera = wb_robot_get_device("camera");
  wb_camera_enable(camera, TIME_STEP);
  wb_camera_move_window(camera, 0, 0);
  width = wb_camera_get_width(camera);
  height = wb_camera_get_height(camera);
  int redMatrix[width][height];
  while(wb_robot_step(TIME_STEP)!= -1) {
    /* This is used to refresh the camera. */
    image = wb_camera_get_image(camera);
    left_speed = 0;
    right_speed = 0;

    /* This is used to stop the robot in front of the blob a few seconds. */
    if (pause_counter == 0) {
      red = 0;
      green = 0;
      blue = 0;
      

      printf("%d\t\t%d\t\t%d\n", red, green, blue);
      if (isCentered(redMatrix, width, height, image)) {
        pause_counter = 20;
        wb_camera_save_image(camera, "frame.png", 100);
        printf("Image saved!\n");
        return 0;
      } else {
        left_speed = -SPEED;
        right_speed = SPEED;
      }
    } else {
      printf("PC");
      pause_counter--;
        /* 
         * We need to move before we start to analyse the image because if we
         * do not, we will have exactly the same image (the one which has 
         * found the blob) on our camera and we will stop again.
         */
      if (pause_counter <= 3) {
        left_speed = -SPEED;
        right_speed = SPEED;
      }
    }
    /* Set the motor speeds. */
    wb_differential_wheels_set_speed(left_speed, right_speed);
  }

  wb_robot_cleanup();

  return 0;
}
